require 'securerandom' # justo to generate voucher code

require_relative 'model/product'
require_relative 'model/membership'
require_relative 'model/customer'
require_relative 'model/address'
require_relative 'model/order'
require_relative 'model/order_item'
require_relative 'model/invoice'
require_relative 'model/payment'
require_relative 'model/credit_card'
require_relative 'model/shipping'

# Handler Rules
require_relative 'handler/order_handler'
require_relative 'handler/rules/book_rule'
require_relative 'handler/rules/digital_rule'
require_relative 'handler/rules/membership_rule'
require_relative 'handler/rules/physical_rule'


p '=========================='
p '===========BOOK==========='
p '=========================='
foolano = Customer.new('Fuz Bar')
book = Product.new(name: 'Awesome book', type: :digital)
book_order = Order.new(foolano)
book_order.add_product(book)

payment_book = Payment.new(order: book_order, payment_method: CreditCard.fetch_by_hashed('43567890-987654367'))
payment_book.pay
p payment_book.paid? # < true
p payment_book.order.items.first.product.type

# now, how to deal with shipping rules then?
book_order_handler = OrderHandler.new(payment_book)
book_order_handler.processShippingRules

# Digital Example
p '=========================='
p '=========DIGITAL=========='
p '=========================='
foolano = Customer.new('Fuz Bar')
book = Product.new(name: 'Awesome book', type: :digital)
book_order = Order.new(foolano)
book_order.add_product(book)

payment_book = Payment.new(order: book_order, payment_method: CreditCard.fetch_by_hashed('43567890-987654367'))
payment_book.pay
p payment_book.paid? # < true
p payment_book.order.items.first.product.type

# now, how to deal with shipping rules then?
book_order_handler = OrderHandler.new(payment_book)
book_order_handler.processShippingRules
p "#{foolano.name} voucher: #{foolano.vouchers.first}"


p '=========================='
p '=========PHYSICAL========='
p '=========================='
broom = Product.new(name: 'broom', type: :physical)
broom_order = Order.new(foolano)
broom_order.add_product(broom)

payment_broom = Payment.new(order: broom_order, payment_method: CreditCard.fetch_by_hashed('43567890-987654367'))
payment_broom.pay
p payment_broom.paid? # < true
p payment_broom.order.items.first.product.type

# now, how to deal with shipping rules then?
broom_order_handler = OrderHandler.new(payment_broom)
broom_order_handler.processShippingRules
p "#{foolano.name} voucher: #{foolano.vouchers.first}"
p broom_order_handler.order.shipping.label

p '=========================='
p '=======MEMBERSHIP========='
p '=========================='
plus_member = Product.new(name: 'Member Plus', type: :membership)
plus_member_order = Order.new(foolano)
plus_member_order.add_product(plus_member)

plus_member_payment = Payment.new(order: plus_member_order, payment_method: CreditCard.fetch_by_hashed('43567890-987654367'))
plus_member_payment.pay
p plus_member_payment.paid? # < true
p plus_member_payment.order.items.first.product.type

# now, how to deal with shipping rules then?
plus_member_handler = OrderHandler.new(plus_member_payment)
plus_member_handler.processShippingRules
