class Shipping
  attr_reader :label, :address

  def initialize(label, address)
    @label = label
    @address = address
  end
end