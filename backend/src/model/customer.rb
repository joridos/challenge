class Customer
  attr_accessor :name, :vouchers

  def initialize (name)
    @name = name
    @vouchers = []
  end

  def add_voucher(voucher)
    @vouchers << voucher
  end
end