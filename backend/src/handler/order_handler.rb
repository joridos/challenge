class OrderHandler
  attr_reader :payment

  def initialize(payment)
    @payment = payment
    @order = payment.order
  end

  def processShippingRules
    raise 'NotPaid' unless @payment.paid?
    startOrderRuleByOrderItem
  end

  private

    def startOrderRuleByOrderItem
      @order.items.each do |order_item|
        initialiseOrderRuleByOrderItem(order_item)
      end
    end

  def initialiseOrderRuleByOrderItem(order_item)
    klass = Object.const_get("#{formatRuleClassName(order_item.itemType)}Rule")
    klass.new(@order).processShippingByOrderItem(order_item)
  end

  def formatRuleClassName(class_name)
    class_name.to_s.split('_').map(&:capitalize).join
  end

end