class PhysicalRule
  def initialize(order)
    @order = order
  end

  def processShippingByOrderItem(order_item)
    shipping_label = getShippingLabelByOrder(order_item.order)
    order_shipping = Shipping.new shipping_label, order_item.order.address
    order_item.order.shipping = order_shipping
  end

  private

  def getShippingLabelByOrder(order)
    "Prepare shipping to #{order.address.zipcode}"
  end

end