class BookRule
  def initialize(order)
    @order = order
  end

  def processShippingByOrderItem(order_item)
    shipping_label = getShippingLabelByOrder(order_item.order)
    Shipping.new shipping_label, order_item.order.address
  end

  private

  def getShippingLabelByOrder(order)
    p 'No tax involved'
  end
end