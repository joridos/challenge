class DigitalRule
  def initialize(order)
    @order = order
  end

  def processShippingByOrderItem(order_item)
    addNewVoucherToCustomer
    sendOrderEmailToCustomer(order_item.order)
  end

  private

  def getShippingLabelByOrder(order)
    "Shipping...#{order.address}"
  end

  def sendOrderEmailToCustomer(order)
    p "sending customer mail...."
    p "Hello #{order.customer.name}, order has been processed."
  end

  def addNewVoucherToCustomer
    voucher = SecureRandom.hex
    p "add voucher #{voucher} to customer...."
    @order.customer.add_voucher(voucher)
  end
end