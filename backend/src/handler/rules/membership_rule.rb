class MembershipRule
  def initialize(order)
    @order = order
  end

  def processShippingByOrderItem(order_item)
    enableMembershipToCustomer order_item.order.customer
  end

  private

  def enableMembershipToCustomer(customer)
    p "Customer #{customer.name} enabled as a membership"
  end
end